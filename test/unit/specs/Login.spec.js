import Vue from 'vue'
import Login from '@/components/Login'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import authModule from '../../../src/store/modules/auth'
import todoModule from '../../../src/store/modules/todo'
import { mount } from 'avoriaz'

Vue.use(Vuex)

describe('Login.vue', () => {
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      plugins: [createPersistedState()],
      modules: {
        auth: authModule,
        todo: todoModule
      }
    })
  })
  it('has a created hook', () => {
    expect(Login.created).to.be.a('function')
  })
  it('sets the correct default data', () => {
    expect(Login.data).to.be.a('function')
    const defaultData = Login.data()
    expect(defaultData).to.deep.equal({
      username: '',
      isCalling: false,
      errors: {
        notFound: false
      }
    })
  })
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Login)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('label').textContent).to.equal('Username')
    expect(vm.$el.querySelector('[type="submit"]').textContent).to.equal('Login')
  })
  it('checks if error message was cleared', () => {
    const Constructor = Vue.extend(Login)
    const vm = new Constructor().$mount()
    expect(Login.methods.clearError).to.be.a('function')
    vm.errors.notFound = true
    vm.clearError()
    expect(vm.errors.notFound).to.be.equal(false)
  })
  it('checks if api call done properly', () => {
    const wrapper = mount(Login, { store })
    expect(Login.methods.login).to.be.a('function')
    wrapper.vm.username = 'Antonette'
    return wrapper.vm.login().then(() => {
      expect(wrapper.vm.errors.notFound).to.be.equal(false)
    })
  })
  it('checks if api call returns error', () => {
    const wrapper = mount(Login, { store })
    expect(Login.methods.login).to.be.a('function')
    wrapper.vm.username = 'Antone'
    return wrapper.vm.login().then(() => {
      expect(wrapper.vm.errors.notFound).to.be.equal(true)
    })
  })
})
