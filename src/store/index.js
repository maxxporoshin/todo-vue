import Vue from 'vue'
import Vuex from 'vuex'
import authModule from './modules/auth'
import todoModule from './modules/todo'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    auth: authModule,
    todo: todoModule
  }
})
