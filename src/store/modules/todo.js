import axios from 'axios'
import config from '@/config'

export default {
  state: {
    todos: []
  },

  mutations: {
    setTodos: (state, todos) => {
      state.todos = todos
    },
    insertTodo: (state, todo) => {
      state.todos.splice(0, 0, todo)
    }
  },

  actions: {
    getTodos: ({ rootState, commit }) => {
      return axios.get(config.api + '/todos',
          { params: { userId: rootState.auth.user.id } })
        .then(res => res.data)
        .then(todos => {
          commit('setTodos', todos)
        })
    },
    createTodo ({ commit }, todo) {
      return axios.post(config.api + '/todos')
        .then(res => res.data.id)
        .then(id => {
          todo.id = id
          commit('insertTodo', todo)
        })
    },
    updateTodo ({ commit, state }, todo) {
      return axios.put(config.api + '/todos/' + todo.id, todo)
        .then(res => res.data)
        .then(todo => {
          let todos = state.todos.map(t => t.id === todo.id ? todo : t)
          commit('setTodos', todos)
        })
    }
  }
}
