import axios from 'axios'
import config from '@/config'

export default {
  state: {
    user: null,
    authenticated: false
  },

  getters: {
    currentUser: state => state.user,
    authenticated: state => state.authenticated
  },

  mutations: {
    setUser: (state, user) => {
      state.user = user
      state.authenticated = true
    },
    removeUser: (state) => {
      state.user = null
      state.authenticated = false
    }
  },

  actions: {
    login ({ state, commit }, username) {
      return axios.get(config.api + '/users',
          { params: { username: username } })
        .then(res => res.data)
        .then(users => {
          if (!users.length) {
            return Promise.reject({ ok: false, status: 404, message: 'Not found' })
          }
          commit('setUser', users[0])
        })
    },
    logout ({ commit }) {
      commit('removeUser')
    }
  }
}
