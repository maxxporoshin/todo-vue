// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'vue-material/dist/vue-material.css'

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueMaterial from 'vue-material'

import TodoList from './components/todo/TodoList'

Vue.use(VueMaterial)
Vue.material.registerTheme('default', {
    primary: {
    color: 'teal',
    hue: 'A400',
    textColor: 'white'
  },
  accent: {
    color: 'deep-orange',
    hue: '400',
    textColor: 'white'
  },
  warn: {
    color: 'red',
    hue: 300
  }
})

Vue.config.productionTip = false

Vue.component('todo-list', TodoList)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
